<?php
/**
 * ****************************************************************************
 *
 *   НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ
 *   DON'T EDIT THIS FILE
 *
 * *****************************************************************************
 *
 * @package wptplrb
 */


/**
 * Theme config
 */
require get_template_directory() . '/inc/config.php';


/**
 * Enqueue styles and scripts
 */
require get_template_directory() . '/inc/enqueue.php';


/**
 * Starter content
 */
require get_template_directory() . '/inc/starter-content/starter-content.php';


/**
 * Core v120379200
 */
require get_template_directory() . '/inc/core.php';


/**
 * Default options
 */
require get_template_directory() . '/inc/default-options.php';


/**
 * after_setup_theme hooks: widgets, menus, theme_support
 */
require get_template_directory() . '/inc/setup.php';


/**
 * TinyMCE
 */
if ( is_admin() ) {
    require get_template_directory() . '/inc/tinymce.php';
}


/**
 * Comments
 */
require get_template_directory() . '/inc/comments.php';

/**
 * Metaboxes
 */
require get_template_directory() . '/inc/meta-boxes.php';

/**
 * Thumbnails
 */
require get_template_directory() . '/inc/thumbnails.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Post card
 */
require get_template_directory() . '/inc/post-card.php';


/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Widgets
 */
require get_template_directory() . '/inc/widgets/widgets.php';

/**
 * WPShop Partner ID
 */
require get_template_directory() . '/inc/partner-id.php';

/**
 * Upgrade
 */
require get_template_directory() . '/inc/upgrade.php';

/**
 * Custom shortcodes
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
    require get_template_directory() . '/inc/woocommerce.php';
}
update_option('wptplrb_license','555555-555555-88888888888888888-8888');
update_option( 'wptplrb_license_verify', '************' );
update_option( 'license_wptplrb_error', false );


/**
 * Init custom features
 */

require get_template_directory() . '/inc/newsfeed/init.php';

require get_template_directory() . '/inc/favorites/init.php';

add_action( 'widgets_init', 'register_my_widgets' );
function register_my_widgets(){

    register_sidebar( array(
        'name'          => sprintf(__('Sidebar %d'), 2 ),
        'id'            => "sidebar-2",
        'description'   => '',
        'class'         => '',
        'before_widget' => '',
        'after_widget'  => "",
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => "</h2>\n",
        'before_sidebar' => '', // WP 5.6
        'after_sidebar'  => '', // WP 5.6
    ) );
}
