<?php


// Rest-функции
add_action('rest_api_init', function () {

    register_rest_route('favorites/v1', '/add_favorite_post/', array(
        'methods'  => 'POST',
        'callback' => 'add_favorite_post',
        'args'     => array(
            'post' => array(
                'type'    => 'integer',
                'required' => true
            ),
        ),
    ));
    register_rest_route('favorites/v1', '/remove_favorite_post/', array(
        'methods'  => 'POST',
        'callback' => 'remove_favorite_post',
        'args'     => array(
            'post' => array(
                'type'    => 'integer',
                'required' => true
            ),
        ),
    ));
});

function add_favorite_post(WP_REST_Request $request) {
    $post = $request->get_param('post');
    // TODO проверка на существование поста и типа записи
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $favorites = new Favorites($user->ID);
        $favorites->add($post);
        return 'ok';
    } else {
        return 'user not login';
    }
}

function remove_favorite_post(WP_REST_Request $request) {
    $post = $request->get_param('post');
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $favorites = new Favorites($user->ID);
        $favorites->remove($post);
        return 'ok';
    } else {
        return 'user not login';
    }
}