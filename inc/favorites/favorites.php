<?php



class Favorites
{
    public $list;
    public $user_id;
    public function __construct($user_id)
    {
        $this->user_id = $user_id;
        $this->list = get_field('favorites', 'user_'.$user_id);
    }
    

    public static function get_page($page = 1, $category = 1) {
        
    }

    public function add($post_id) {
        $flag = true;
        if (is_array($this->list) && count($this->list) > 0) {
            foreach ($this->list as $item) {
                if ($item['post'] == $post_id) {
                    $flag = false;
                }
            }
        }
        if ($flag) {
            $this->list[] = [
                'post' => $post_id,
            ];
            update_field('favorites',$this->list, 'user_'.$this->user_id);
        }
        return true;
    }
    
    public function remove($post_id) {
        if (is_array($this->list) && count($this->list) > 0) {
            $item_id = 0;
            foreach ($this->list as $key=>$item) {
                if ($item['post'] == $post_id) {
                    $item_id = $key;
                    break;
                }
            }
            unset($this->list[$item_id]);
            update_field('favorites',$this->list, 'user_'.$this->user_id);
        } 
        return true;
    }

    public static function is_favorite($user_id, $post_id) {
        $favorites = new Favorites($user_id);
        $flag = false;
        if (is_array($favorites->list) && count($favorites->list) > 0) {
            foreach ($favorites->list as $key => $item) {
                if ($item['post'] == $post_id) {
                    $flag = true;
                    break;
                }
            }
        }
        return $flag;
    }

}
