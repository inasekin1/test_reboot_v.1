<?php

use Wptplrb\Core\AdminNotices;
use Wptplrb\Core\Core;
use Wptplrb\Core\ThemeUpdater;
use Wptplrb\Core\ThemeOptions;
use Wptplrb\Core\ThemeSettings;
use Wptplrb\Core\Helper;
use Wptplrb\Core\Advertising;
use Wptplrb\Core\Sitemap;
use Wptplrb\Core\Template;
use Wptplrb\Core\ContactForm;
use Wptplrb\Core\Shortcodes;
use Wptplrb\Core\Breadcrumbs;
use Wptplrb\Core\Social;
use Wptplrb\Core\TableOfContents;
use Wptplrb\Core\Customizer\Customizer;
use Wptplrb\Core\Fonts;
use Wptplrb\Core\StarRating;
use Wptplrb\Core\MetaBoxTaxonomy;
use Wptplrb\Core\Partner;
use Wptplrb\Core\ViewsCounter;


require get_template_directory() . '/vendor/autoload.php';


/**
 * Theme options
 */
$wptplrb_theme_options = new ThemeOptions();
$wptplrb_theme_options->text_domain   = THEME_TEXTDOMAIN;
$wptplrb_theme_options->theme_slug    = THEME_SLUG;
$wptplrb_theme_options->settings_name = THEME_SLUG . '_settings';
$wptplrb_theme_options->option_name   = THEME_SLUG . '_options';
$wptplrb_theme_options->theme_name    = THEME_NAME;
$wptplrb_theme_options->theme_title   = THEME_TITLE;
$wptplrb_theme_options->updater_url   = THEME_UPDATE_URL;

$wptplrb_theme_options->license           = THEME_SLUG . '_license';
$wptplrb_theme_options->license_verify    = THEME_SLUG . '_license_verify';
$wptplrb_theme_options->license_error     = THEME_SLUG . '_license_error';

$wptplrb_theme_options->settings_page_title = __( 'Theme Settings', THEME_TEXTDOMAIN );
$wptplrb_theme_options->settings_menu_title = __( 'Theme Settings', THEME_TEXTDOMAIN );


/**
 * Init
 */
$wptplrb_helper              = new Helper( $wptplrb_theme_options );
$wptplrb_core                = new Core( $wptplrb_theme_options );
$wptplrb_theme_settings      = new ThemeSettings( $wptplrb_theme_options );
$wptplrb_theme_updater       = new ThemeUpdater( $wptplrb_theme_options );
$wptplrb_customizer          = new Customizer( $wptplrb_theme_options );
$wptplrb_template            = new Template( $wptplrb_theme_options );
$wptplrb_breadcrumbs         = new Breadcrumbs( $wptplrb_theme_options );
$wptplrb_sitemap             = new Sitemap( $wptplrb_theme_options );
$class_advertising          = new Advertising( $wptplrb_theme_options );
$class_shortcodes           = new Shortcodes( $wptplrb_theme_options );
$wptplrb_social              = new Social( $wptplrb_theme_options );
$wptplrb_table_of_contents   = new TableOfContents( $wptplrb_theme_options );
$wptplrb_rating              = new StarRating( $wptplrb_theme_options );
$wptplrb_contact_form        = new ContactForm( $wptplrb_theme_options );
$wptplrb_fonts               = new Fonts( $wptplrb_theme_options );
$wptplrb_partner             = new Partner( $wptplrb_theme_options );
$wptplrb_admin_notices       = new AdminNotices( $wptplrb_theme_options );
$wptplrb_views_counter       = new ViewsCounter( $wptplrb_core, $wptplrb_theme_options, $wptplrb_admin_notices );

if ( class_exists( 'WP_CLI' ) ) {
    $GLOBALS['wptplrb_helper']            = $wptplrb_helper;
    $GLOBALS['wptplrb_core']              = $wptplrb_core;
    $GLOBALS['wptplrb_theme_settings']    = $wptplrb_theme_settings;
    $GLOBALS['wptplrb_theme_updater']     = $wptplrb_theme_updater;
    $GLOBALS['wptplrb_customizer']        = $wptplrb_customizer;
    $GLOBALS['wptplrb_template']          = $wptplrb_template;
    $GLOBALS['wptplrb_breadcrumbs']       = $wptplrb_breadcrumbs;
    $GLOBALS['wptplrb_sitemap']           = $wptplrb_sitemap;
    $GLOBALS['class_advertising']        = $class_advertising;
    $GLOBALS['class_shortcodes']         = $class_shortcodes;
    $GLOBALS['wptplrb_social']            = $wptplrb_social;
    $GLOBALS['wptplrb_table_of_contents'] = $wptplrb_table_of_contents;
    $GLOBALS['wptplrb_rating']            = $wptplrb_rating;
    $GLOBALS['wptplrb_contact_form']      = $wptplrb_contact_form;
    $GLOBALS['wptplrb_fonts']             = $wptplrb_fonts;
    $GLOBALS['wptplrb_partner']           = $wptplrb_partner;
    $GLOBALS['wptplrb_admin_notices']     = $wptplrb_admin_notices;
    $GLOBALS['wptplrb_views_counter']     = $wptplrb_views_counter;
}


/**
 * Core
 */
$wptplrb_core->minimum_php_version( '5.3' );

$wptplrb_admin_notices->init();


/**
 * Template
 */
$wptplrb_template->init( array(
    'remove_hentry',
    'remove_current_links_from_menu',
    'remove_h_tag_from_navigation',
    'remove_label_archive_title',
    'microformat_image',
    'remove_style_tag',
    'remove_script_tag',
) );
$wptplrb_template->body_class_customizer();


/**
 * MetaBoxTaxonomy
 */
$wptplrb_metabox_taxonomy = new MetaBoxTaxonomy();


/**
 * Shortcodes
 */
$class_shortcodes->init_shortcode( 'button' );
$class_shortcodes->init_shortcode( 'spoiler' );
$class_shortcodes->init_shortcode( 'mask_link' );
$class_shortcodes->init_shortcode( 'social_profiles' );
$class_shortcodes->init_shortcode( 'current_year' );
$class_shortcodes->add_shortcode_support();


add_action( 'after_setup_theme', 'wptplrb_core_setup' );
function wptplrb_core_setup() {

    global $wptplrb_breadcrumbs;
    global $class_advertising;
    global $wptplrb_contact_form;

    $wptplrb_breadcrumbs->set_home_text( __( 'Home', THEME_TEXTDOMAIN ) );

    /**
     * Advertising
     */
    $advertising_positions = apply_filters( THEME_SLUG . '_advertising_positions', array(
        'before_site_content' => array(
            'title' => __( 'After the header and top menu (for the entire width of the site)', THEME_TEXTDOMAIN ),
            'type'  => 'regular',
        ),
        'before_content' => array(
            'type'  => 'before_content',
        ),
        'middle_content' => array(
            'type'  => 'middle_content',
        ),
        'after_content' => array(
            'type'  => 'after_content',
        ),
        'after_p_1' => array(
            'type'  => 'after_p',
        ),
        'after_p_2' => array(
            'type'  => 'after_p',
        ),
        'after_p_3' => array(
            'type'  => 'after_p',
        ),
        'after_p_4' => array(
            'type'  => 'after_p',
        ),
        'after_p_5' => array(
            'type'  => 'after_p',
        ),
        'before_related' => array(
            'title' => __( 'Before related posts', THEME_TEXTDOMAIN ),
            'type'  => 'single',
        ),
        'after_related' => array(
            'title' => __( 'After related posts', THEME_TEXTDOMAIN ),
            'type'  => 'single',
        ),

        'after_site_content' => array(
            'title' => __( 'Before the bottom menu and footer (for the entire width of the site)', THEME_TEXTDOMAIN ),
            'type'  => 'regular',
        ),
    ) );
    $class_advertising->set_positions( $advertising_positions );
    $class_advertising->init();

    /**
     * Contact form
     */
    $fields = array(
        array(
            'name'          => 'contact-name',
            'placeholder'   => __( 'Your name', THEME_TEXTDOMAIN ),
            'required'      => 'required',
        ),
        array(
            'name'          => 'contact-email',
            'type'          => 'email',
            'placeholder'   => __( 'Your e-mail', THEME_TEXTDOMAIN ),
            'required'      => 'required',
        ),
        array(
            'name'          => 'contact-subject',
            'placeholder'   => __( 'Your subject', THEME_TEXTDOMAIN ),
        ),
        array(
            'tag'           => 'textarea',
            'name'          => 'contact-message',
            'placeholder'   => __( 'Message', THEME_TEXTDOMAIN ),
            'required'      => 'required',
        ),
    );
    $fields = apply_filters( THEME_SLUG . '_contact_form_fields' , $fields );
    $wptplrb_contact_form->create_fields( $fields );

}

add_filter( 'wptplrb_text_before_submit', 'contact_form_text_before_submit' );

function contact_form_text_before_submit() {
    global $wptplrb_core;
    $contact_form_text_before_submit = $wptplrb_core->get_option( 'contact_form_text_before_submit' );

    if ( ! empty( $contact_form_text_before_submit ) ) {
        return '<div class="contact-form-notes-after">'. $contact_form_text_before_submit .'</div>';
    }
}


/**
 * Rating
 */
if ( defined('DOING_AJAX') ) {
    $wptplrb_rating->ajax_actions();
}


/**
 * Fonts
 */
$wptplrb_fonts->preloading_fonts( get_template_directory_uri() . '/assets/fonts/wptplrb-core.ttf' );