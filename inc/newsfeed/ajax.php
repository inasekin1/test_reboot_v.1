<?php

// Rest-функции
add_action('rest_api_init', function () {

    register_rest_route('newsfeed/v1', '/get_news/', array(
        'methods'  => 'POST',
        'callback' => 'get_news',
        'args'     => array(
            'page' => array(
                'type'    => 'integer',
                'default' => 1
            ),
            'category' => array(
                'type'    => 'integer',
                'default' => 0
            )
        ),
    ));

    register_rest_route('newsfeed/v1', '/get_pages_count/', array(
        'methods'  => 'POST',
        'callback' => 'get_news_pages_count',
    ));
});

function get_news(WP_REST_Request $request)
{
    $page = $request->get_param('page');
    $category = $request->get_param('category');
    return Newsfeed::get_page($page,$category);
}

function get_news_pages_count(WP_REST_Request $request)
{
    return Newsfeed::get_pages_count();
}