<?php



class Newsfeed
{

    public $id = 0;
    public $title = "";
    public $link = "";
    public $image;
    public $description = "";
    public $date;

    const posts_per_page = 1;

    /**
     * __construct
     *
     * @param  int $id Идентификатор продукта
     * @return void
     */
    public function __construct($id)
    {
        $post = get_post($id, 'OBJECT');
        if ($post->post_type == 'post') {
            $this->id = $id;
        }
    }

    /**
     * get_card - получение карточки новости
     *
     * @return void
     */
    public function get_card()
    {
        global $wptplrb_core;
        global $wptplrb_helper;
        global $wptplrb_template;
        // Получение названия категории
        $categories = get_the_category($this->id);

        $html = '<div class="post-card post-card--standard w-animate" itemscope="" itemtype="http://schema.org/BlogPosting">
                   <div class="post-card__thumbnail">
                        <a href="'.get_permalink($this->id).'">';
        if (has_post_thumbnail($this->id)) {
            $thumbnail_id = get_post_thumbnail_id($this->id);
            $image = wp_get_attachment_image_src($thumbnail_id,'large');
            $html .= '<img src="'.$image[0].'" class="attachment-wptplrb_standard size-wptplrb_standard wp-post-image" loading="lazy" itemprop="image" width="'.$image[1].'" height="'.$image[2].'">';
        }
        $html .= '         <span itemprop="articleSection" class="post-card__category">'.$categories[0]->name.'</span>
                        </a>
                    </div>
                    <div class="post-card__title" itemprop="name">
                        <span itemprop="headline">
                            <a href="'.get_permalink($this->id).'">'.get_the_title($this->id).'</a>
                        </span>
                    </div>
                    <div class="post-card__meta">
                        <span class="post-card__date">
                            <time itemprop="datePublished" datetime="'. get_the_time( 'Y-m-d', $this->id ) .'">'.get_the_date('d.m.Y', $this->id).'</time>
                        </span>
                        <span class="post-card__comments">' . get_comments_number($this->id) . '</span>
                        <span class="post-card__views">'.$wptplrb_helper->rounded_number( $wptplrb_template->get_views($this->id) ).'</span>
                    </div>
                    <div class="post-card__description" itemprop="articleBody">'.get_the_excerpt($this->id).'</div>
                    <meta itemprop="author" content="admin">
                    <meta itemscope="" itemprop="mainEntityOfPage" itemtype="https://schema.org/WebPage" itemid="'.get_permalink($this->id).'" content="'.get_the_title($this->id).'">
                    <meta itemprop="dateModified" content="'. get_the_time( 'Y-m-d', $this->id ) .'">
                    '.$wptplrb_template->get_microdata_publisher().'
                  </div>';
        return $html;
    }

    public static function get_page($page = 1, $category = 1) {
        $args = [
            'post_type' => 'post',
            'paged'    => $page,
            'posts_per_page' => Newsfeed::posts_per_page,
            'cat' => $category
        ];
        $query = new WP_Query($args);
        $html = '';
        if ($query->have_posts()) {
            while ($query->have_posts()) :
                $query->the_post();
                $news = new Newsfeed(get_the_ID());
                $html = $html.$news->get_card();
            endwhile;
        } else {
            $html = 'Записи не найдены';
        }
        wp_reset_postdata();
        return $html;
    }

    public static function get_pages_count($category_id = 1) {
        $category = get_category($category_id);
        $count = $category->count;
        $pages = intval($count / Newsfeed::posts_per_page);
        if ($count % Newsfeed::posts_per_page > 0) {
            $pages = $pages + 1;
        }
        return $pages;
    }

}
