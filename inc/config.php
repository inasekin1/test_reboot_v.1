<?php
/**
 * ****************************************************************************
 *
 *   НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ
 *   DON'T EDIT THIS FILE
 *
 * *****************************************************************************
 *
 * @package wptplrb
 */

$original_theme = $theme = wp_get_theme();
if ( $theme->parent() ) {
    $theme = $theme->parent();
}
define( 'THEME_VERSION', $theme->get( 'Version' ) );
define( 'THEME_ORIGINAL_VERSION', $original_theme->get( 'Version' ) );
define( 'THEME_TEXTDOMAIN', $theme->get( 'TextDomain' ) );
define( 'THEME_NAME', 'wptplrb' );
define( 'THEME_TITLE', 'Wptplrb' );
define( 'THEME_SLUG', 'wptplrb' );
define( 'THEME_API_URL', 'https://wptplrb.ru/api.php' );
define( 'THEME_UPDATE_URL', 'https://api.wpgenerator.ru/wp-update-server/' );
