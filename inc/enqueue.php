<?php
/**
 * ****************************************************************************
 *
 *   НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ
 *   DON'T EDIT THIS FILE
 *
 *   После обновления Вы потереяете все изменения. Используйте дочернюю тему
 *   After update you will lose all changes. Use child theme
 *
 *   https://docs.wptplrb.ru/start/child-themes
 *
 * *****************************************************************************
 *
 * @package wptplrb
 */


load_theme_textdomain( THEME_TEXTDOMAIN, get_template_directory() . '/languages' );


/**
 * Enqueue scripts and styles.
 */
function wptplrb_enqueue() {
    global $wptplrb_fonts;
    global $wptplrb_core;

    $style_version = apply_filters( THEME_SLUG . '_style_version', THEME_VERSION );


    // get list of font families options
    $fonts_options = array(
        'typography_body', 'typography_site_title', 'typography_site_description', 'typography_menu_links',
    );

    // get list of font families
    $fonts_list = array();
    foreach ( $fonts_options as $fonts_option ) {
        $option = $wptplrb_core->get_option( $fonts_option );
        $option_decode = json_decode( $option, true );
        if ( ! empty( $option_decode['font-family'] ) ) {
            $fonts_list[] = $option_decode['font-family'];
        } else {
            // back compat, when option is font family
            $fonts_list[] = $option;
        }
    }
    $google_fonts = $wptplrb_fonts->get_enqueue_link( $fonts_list );

    // enqueue link
    if ( ! empty( $google_fonts ) ) {
        wp_enqueue_style( 'google-fonts', $google_fonts, false );
    }


    wp_enqueue_style( THEME_NAME . '-style',   get_template_directory_uri() . '/assets/css/style.min.css', array(), $style_version );
    wp_enqueue_style( THEME_NAME . '-custom',   get_template_directory_uri() . '/assets/css/custom.css', array(), $style_version );
    wp_enqueue_style( THEME_NAME . '-plugin-admin-easy-table-contents',   get_template_directory_uri() . '/assets/css/plugins/easy-table-of-contents/css/admin.min.css', array(), $style_version );
    wp_enqueue_style( THEME_NAME . '-plugin-screen-easy-table-contents',   get_template_directory_uri() . '/assets/css/plugins/easy-table-of-contents/css/screen.min.css', array(), $style_version );
    wp_enqueue_script( 'wp-api' );
    wp_enqueue_script( THEME_NAME . '-scripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array('jquery'), $style_version, true );
    wp_enqueue_script( 'swiper', get_template_directory_uri() . '/assets/js/plugins/swiper.min.js', array(), $style_version, true );
    wp_enqueue_script( THEME_NAME . '-custom', get_template_directory_uri() . '/assets/js/custom.js', array('jquery','swiper'), $style_version, true );

    wp_localize_script( THEME_NAME . '-scripts', 'settings_array', array(
            'rating_text_average' => __( 'average', THEME_TEXTDOMAIN ),
            'rating_text_from'    => __( 'from', THEME_TEXTDOMAIN ),
            'lightbox_display'    => $wptplrb_core->get_option( 'lightbox_display' ),
            'sidebar_fixed'       => $wptplrb_core->get_option( 'sidebar_fixed' ),
        )
    );

    // swiper
    $slider_count_mod = $wptplrb_core->get_option( 'slider_count' );
    if ( ( is_front_page() || is_home() ) && $slider_count_mod != 0 ) {
        wp_enqueue_script( THEME_NAME . '-swiper', get_template_directory_uri() . '/assets/js/plugins/swiper.min.js', array('jquery'), $style_version, true );
    }

    // ajax
    wp_localize_script( THEME_NAME . '-scripts' , 'wps_ajax', array(
        'url'   => admin_url( 'admin-ajax.php' ),
        'nonce' => wp_create_nonce( 'wptplrb-nonce' )
    ) );

    // comment reply script
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'wptplrb_enqueue' );



/**
 * Enqueue admin scripts and styles.
 */
function wptplrb_enqueue_admin() {
    wp_enqueue_style( THEME_NAME . '-admin-style', get_template_directory_uri() . '/assets/css/admin.min.css', array(), null );
    wp_enqueue_script( THEME_NAME . '-admin-scripts', get_template_directory_uri() . '/assets/js/admin.min.js', array('jquery'), null, true );
}
add_action( 'admin_enqueue_scripts', 'wptplrb_enqueue_admin' );


/**
 * Editor styles
 */
function wptplrb_add_editor_style() {
    add_editor_style( 'assets/css/editor-styles.min.css' );
}
add_action( 'current_screen', 'wptplrb_add_editor_style' );


/**
 * Gutenberg scripts and styles
 */
function wptplrb_enqueue_gutenberg() {
	wp_enqueue_script(
		THEME_SLUG . '-gutenberg',
        get_template_directory_uri() . '/assets/js/gutenberg.min.js',
		array( 'wp-blocks', 'wp-dom' ),
		THEME_VERSION,
		true
	);

	wp_enqueue_style(
		THEME_SLUG . '-gutenberg',
        get_template_directory_uri() . '/assets/css/gutenberg.min.css',
		array(),
		THEME_VERSION
	);
}
add_action( 'enqueue_block_editor_assets', 'wptplrb_enqueue_gutenberg' );