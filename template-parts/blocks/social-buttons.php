<?php
/**
 * ****************************************************************************
 *
 *   НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ
 *   DON'T EDIT THIS FILE
 *
 *   После обновления Вы потереяете все изменения. Используйте дочернюю тему
 *   After update you will lose all changes. Use child theme
 *
 *   https://support.wptplrb.ru/docs/general/child-themes/
 *
 * *****************************************************************************
 *
 * @package wptplrb
 */

global $wptplrb_core;
global $wptplrb_social;

$share_buttons = $wptplrb_core->get_option( 'share_buttons' );
$share_buttons = explode(',', $share_buttons);

echo '<div class="social-buttons">';
$wptplrb_social->share_buttons( $share_buttons, array( 'show_label' => false, 'show_counters' => $wptplrb_core->get_option( 'share_buttons_counters' ) ) );
echo '</div>';