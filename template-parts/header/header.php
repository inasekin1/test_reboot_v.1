<?php
/**
 * ****************************************************************************
 *
 *   НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ
 *   DON'T EDIT THIS FILE
 *
 *   После обновления Вы потереяете все изменения. Используйте дочернюю тему
 *   After update you will lose all changes. Use child theme
 *
 *   https://support.wptplrb.ru/docs/general/child-themes/
 *
 * *****************************************************************************
 *
 * @package wptplrb
 */

global $wptplrb_core;

$header_block_order = $wptplrb_core->get_option( 'header_block_order' );
$header_block_order = explode( ',', $header_block_order );
?>

<?php do_action( THEME_SLUG . '_before_header' ) ?>

<header id="masthead" class="site-header <?php $wptplrb_core->the_option( 'header_width' ) ?>" itemscope itemtype="http://schema.org/WPHeader">
    <div class="site-header-inner <?php $wptplrb_core->the_option( 'header_inner_width' ) ?>">

        <div class="humburger js-humburger"><span></span><span></span><span></span></div>

        <?php foreach ( $header_block_order as $order ) {

            if ( $order == 'site_branding' ) {
                get_template_part( 'template-parts/header/site', 'branding' );
            }

            $header_html_block_1 = $wptplrb_core->get_option( 'header_html_block_1' );
            if ( $order == 'header_html_block_1' && ! empty ( $header_html_block_1 ) ) { ?>
                <div class="header-html-1">
                    <?php echo do_shortcode( $header_html_block_1 ) ?>
                </div>
            <?php }

            if ( $order == 'header_social' ) {
                get_template_part( 'template-parts/blocks/social', 'links' );
            }

            if ( $order == 'top_menu' ) {
                get_template_part( 'template-parts/navigation/top' );
            }

            $header_html_block_2 = $wptplrb_core->get_option( 'header_html_block_2' );
            if ( $order == 'header_html_block_2' && ! empty( $header_html_block_2 ) ) { ?>
                <div class="header-html-2">
                    <?php echo do_shortcode( $header_html_block_2 ) ?>
                </div>
            <?php }

            if ( $order == 'header_search' ) { ?>
                <div class="header-search">
                    <span class="search-icon js-search-icon"></span>
                </div>
            <?php }

        } ?>
		<?=do_shortcode('[geo_text]'); ?>
    </div>
	<?php
		$advertisement_html_block = $wptplrb_core->get_option( 'advertisement_html_block' );
	?>
	<div class="advertisement-block">
		<?= do_shortcode( $advertisement_html_block ); ?>
	</div>
</header><!-- #masthead -->

<?php do_action( THEME_SLUG . '_after_header' ) ?>