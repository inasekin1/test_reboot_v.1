<?php
/**
 * ****************************************************************************
 *
 *   НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ
 *   DON'T EDIT THIS FILE
 *
 *   После обновления Вы потереяете все изменения. Используйте дочернюю тему
 *   After update you will lose all changes. Use child theme
 *
 *   https://support.wptplrb.ru/docs/general/child-themes/
 *
 * *****************************************************************************
 *
 * @package wptplrb
 */

global $wptplrb_core;

$structure_home_h1 = $wptplrb_core->get_option( 'structure_home_h1' );
$structure_home_text = $wptplrb_core->get_option( 'structure_home_text' );

if ( ! empty( $structure_home_h1 ) || ! empty( $structure_home_text ) || is_customize_preview() ) {

    echo '<div class="home-content">';

    if ( ! empty( $structure_home_h1 ) || is_customize_preview() ) {
        echo '<h1 class="home-header">' . $structure_home_h1 . '</h1>';
    }
    if ( ( ! empty( $structure_home_text ) || is_customize_preview()) && ! is_paged() ) {
        echo '<div class="home-text">' . do_shortcode( wpautop( $structure_home_text ) ) . '</div>';
    }

    echo '</div>';

}
