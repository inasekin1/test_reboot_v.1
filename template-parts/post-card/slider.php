<?php
/**
 * ****************************************************************************
 *
 *   НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ
 *   DON'T EDIT THIS FILE
 *
 *   После обновления Вы потереяете все изменения. Используйте дочернюю тему
 *   After update you will lose all changes. Use child theme
 *
 *   https://support.wptplrb.ru/docs/general/child-themes/
 *
 * *****************************************************************************
 *
 * @package wptplrb
 */

global $wptplrb_core;
global $wptplrb_template;
global $wptplrb_helper;

$thumb_url = get_the_post_thumbnail_url( $post, 'full' );

$is_show_category = $wptplrb_core->get_option( 'slider_show_category' );
$is_show_title    = $wptplrb_core->get_option( 'slider_show_title' );
$is_show_excerpt  = $wptplrb_core->get_option( 'slider_show_excerpt' );

?>

    <div class="swiper-slide">

        <a href="<?php the_permalink() ?>">
            <div class="card-slider__image" <?php if ( ! empty( $thumb_url ) ) echo ' style="background-image: url('. $thumb_url .');"' ?>></div>

            <div class="card-slider__body">
                <div class="card-slider__body-inner">
                    <?php if ( $is_show_category ) : ?>
                        <?php echo $wptplrb_template->get_category( [ 'classes' => 'card-slider__category', 'micro' => false, 'link' => false ] ) ?>
                    <?php endif; ?>

                    <?php if ( $is_show_title ) : ?>
                        <div class="card-slider__title"><?php the_title() ?></div>
                    <?php endif; ?>

                    <?php if ( $is_show_excerpt ) : ?>
                        <?php echo '<div class="card-slider__excerpt">'; ?>
                            <?php echo $wptplrb_helper->substring_by_word( strip_tags( get_the_excerpt() ) ); ?>
                        <?php echo '</div>'; ?>
                    <?php endif; ?>
                </div>
            </div>
        </a>

    </div>