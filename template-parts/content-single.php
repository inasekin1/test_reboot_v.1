<?php
/**
 * ****************************************************************************
 *
 *   НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ
 *   DON'T EDIT THIS FILE
 *
 *   После обновления Вы потереяете все изменения. Используйте дочернюю тему
 *   After update you will lose all changes. Use child theme
 *
 *   https://support.wptplrb.ru/docs/general/child-themes/
 *
 * *****************************************************************************
 *
 * @package wptplrb
 */

global $wptplrb_core;
global $wptplrb_template;
global $wptplrb_helper;
global $wptplrb_rating;
global $thumbnail_type;

global $is_show_rating;

$thumb = get_the_post_thumbnail( $post->ID, apply_filters( THEME_SLUG . '_post_thumbnail', THEME_SLUG . '_standard' ), array( 'itemprop' => 'image' ) );

$thumb_url = get_the_post_thumbnail_url( $post, 'full' );

$structure_single_hide  = $wptplrb_core->get_option( 'structure_single_hide' );
if ( ! empty( $structure_single_hide ) ) {
    $structure_single_hide = explode( ',', $structure_single_hide );
    if ( is_array( $structure_single_hide ) ) {
        $structure_single_hide = array_map( 'trim', $structure_single_hide );
    }
} else {
    $structure_single_hide = array();
}

$is_show_thumb          = ( ! in_array( 'thumbnail', $structure_single_hide ) && $wptplrb_core->is_show_element( 'thumbnail' ) );
$is_show_breadcrumbs    = $wptplrb_core->is_show_element( 'breadcrumbs' );
$is_show_title_h1       = $wptplrb_core->is_show_element( 'title_h1' );
$is_show_social_top     = ( ! in_array( 'social_top', $structure_single_hide ) && $wptplrb_core->is_show_element( 'social_top' ) );
$is_show_category       = ( ! in_array( 'category', $structure_single_hide ) );
$is_show_meta           = $wptplrb_core->is_show_element( 'meta' );
$is_show_author         = ( ! in_array( 'author', $structure_single_hide ) );
$is_show_reading_time   = ( ! in_array( 'reading_time', $structure_single_hide ) );
$is_show_views          = ( ! in_array( 'views', $structure_single_hide ) );
$is_show_date           = ( ! in_array( 'date', $structure_single_hide ) );
$is_show_date_modified  = ( ! in_array( 'date_modified', $structure_single_hide ) );
$is_show_excerpt        = ( ! in_array( 'excerpt', $structure_single_hide ) );
$is_show_tags           = ( ! in_array( 'tags', $structure_single_hide ) );
$is_show_rating         = ( ! in_array( 'rating', $structure_single_hide ) && $wptplrb_core->is_show_element( 'rating' ) );
$is_show_social_bottom  = ( ! in_array( 'social_bottom', $structure_single_hide ) && $wptplrb_core->is_show_element( 'social_bottom' ) );
$is_show_author_box     = ( ! in_array( 'author_box', $structure_single_hide ) && $wptplrb_core->is_show_element( 'author_box' ) );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'article-post' ); ?>>

    <?php if ( $thumbnail_type != 'wide' && $thumbnail_type != 'full' && $thumbnail_type != 'fullscreen' ) : ?>

        <?php if ( $is_show_breadcrumbs ) {
            get_template_part( 'template-parts/blocks/breadcrumbs' );
        } ?>

        <?php if ( $is_show_title_h1 ) { ?>
            <?php do_action( THEME_SLUG . '_single_before_title' ) ?>
            <h1 class="entry-title" itemprop="headline"><?php the_title() ?></h1>
            <?php do_action( THEME_SLUG . '_single_after_title' ) ?>
        <?php } ?>

        <?php if ( $is_show_social_top ) { ?>
            <?php get_template_part( 'template-parts/blocks/social', 'buttons' ) ?>
        <?php } ?>

        <?php if ( $is_show_thumb && ! empty( $thumb ) ) : ?>
            <div class="entry-image post-card post-card__thumbnail">
                <?php echo $thumb; ?>
                <?php if ( $is_show_category && is_singular( 'post' ) ) { ?>
                    <span class="post-card__category"><?php echo $wptplrb_template->get_category() ?></span>
                <?php } ?>
            </div>

        <?php endif; ?>

    <?php endif; ?>

    <?php if ( $is_show_meta && ( $is_show_author || $is_show_reading_time || ( $is_show_views && $wptplrb_template->get_views() > 0 ) || $is_show_date || $is_show_date_modified ) ) { ?>
        <div class="entry-meta">
            <?php if ( $is_show_author ) echo '<span class="entry-author" itemprop="author"><span class="entry-label">' . __( 'Author', THEME_TEXTDOMAIN ) . '</span> '; ?>
            <?php if (get_field('author_link')) { echo get_the_author_posts_link(); } else { echo get_the_author(); } ?>
            <?php echo '</span>'; ?>
            <?php if ( $is_show_reading_time ) echo '<span class="entry-time"><span class="entry-label">' . __( 'Reading', THEME_TEXTDOMAIN ) . '</span> ' . wptplrb_read_time() . ' ' . __( 'min', THEME_TEXTDOMAIN ) . '</span>'; ?>
            <?php if ( $is_show_views && $wptplrb_template->get_views() > 0 ) {
                echo '<span class="entry-views">'.
                     '<span class="entry-label">' . __( 'Views', THEME_TEXTDOMAIN ) . '</span> ' .
                     '<span class="js-views-count" data-post_id="' . $post->ID . '">' .
                     $wptplrb_helper->rounded_number( $wptplrb_template->get_views() ) . '</span>'.
                     '</span>';
            } ?>
            <?php if ( $is_show_date ) echo '<span class="entry-date"><span class="entry-label">' . __( 'Published by', THEME_TEXTDOMAIN ) . '</span> <time itemprop="datePublished" datetime="' . get_the_time( 'Y-m-d' ) . '">' . get_the_date() . '</time></span>'; ?>

            <?php if ( $is_show_date_modified ) echo '<span class="entry-date post-last-modified"><span class="entry-label">' . __( 'Modified by', THEME_TEXTDOMAIN ) . '</span> <time itemprop="dateModified" datetime="' . get_the_modified_time( 'Y-m-d' ) . '">' . get_the_modified_date() . '</time></span>'; ?>
            <?php
            if (is_user_logged_in()) {
                $user = wp_get_current_user();
                $is_favorite = Favorites::is_favorite($user->ID, get_the_ID());
            } else {
                $is_favorite = false;
            }
            ?>
            <div class="favorite-button <?php if ($is_favorite) { echo 'favorite-button--liked'; } ?>" data-post-id="<?php echo get_the_ID(); ?>">
                <div class="favorite-button__icon">
                    <div class="favorite-button__icon-like">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M20.205 4.791C19.6536 4.23572 18.9979 3.7949 18.2756 3.49388C17.5532 3.19287 16.7785 3.0376 15.996 3.037C14.5158 3.03724 13.0897 3.59327 12 4.595C10.9104 3.5931 9.48421 3.03705 8.004 3.037C7.22054 3.03781 6.44497 3.19356 5.72194 3.49527C4.9989 3.79698 4.34267 4.23869 3.791 4.795C1.438 7.158 1.439 10.854 3.793 13.207L12 21.414L20.207 13.207C22.561 10.854 22.562 7.158 20.205 4.791V4.791Z"/>
                        </svg>
                    </div>
                    <div class="favorite-button__icon-dislike">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12 4.595C10.9104 3.5931 9.48421 3.03705 8.004 3.037C7.22054 3.03781 6.44498 3.19356 5.72194 3.49527C4.9989 3.79698 4.34267 4.23869 3.791 4.795C1.438 7.158 1.439 10.854 3.793 13.207L11.125 20.539C11.295 20.838 11.623 21.031 12 21.031C12.1548 21.0295 12.3071 20.9917 12.4446 20.9207C12.5822 20.8496 12.7012 20.7473 12.792 20.622L20.207 13.207C22.561 10.853 22.561 7.158 20.205 4.791C19.6536 4.23572 18.9979 3.7949 18.2756 3.49388C17.5532 3.19287 16.7785 3.0376 15.996 3.037C14.5158 3.03724 13.0897 3.59327 12 4.595V4.595ZM18.791 6.205C20.354 7.776 20.355 10.23 18.793 11.793L12 18.586L5.207 11.793C3.645 10.23 3.646 7.776 5.205 6.209C5.965 5.453 6.959 5.037 8.004 5.037C9.049 5.037 10.039 5.453 10.793 6.207L11.293 6.707C11.3858 6.79994 11.496 6.87368 11.6173 6.92399C11.7386 6.9743 11.8687 7.0002 12 7.0002C12.1313 7.0002 12.2614 6.9743 12.3827 6.92399C12.504 6.87368 12.6142 6.79994 12.707 6.707L13.207 6.207C14.719 4.698 17.281 4.702 18.791 6.205V6.205Z"/>
                        </svg>
                    </div>
                </div>
                <div class="favorite-button__text">
                    <?php if (!$is_favorite) { echo 'Добавить в<br><span>избранное</span>'; }
                    else { echo 'Удалить из<br><span>избранного</span>'; }?>

                </div>
            </div>
        </div>
    <?php } ?>

    <?php
    $excerpt = get_the_excerpt();
    if ( has_excerpt() && $is_show_excerpt ) {
        do_action( THEME_SLUG . '_single_before_excerpt' );
        echo '<div class="entry-excerpt">' . $excerpt . '</div>';
        do_action( THEME_SLUG . '_single_after_excerpt' );
    }
    ?>

    <div class="entry-content" itemprop="articleBody">
        <a href="#" class="add-to-favorites" data-post-id="<?php echo get_the_ID(); ?>">Добавить в избранное</a>
        <?php
        do_action( THEME_SLUG . '_single_before_the_content' );
        the_content();
        do_action( THEME_SLUG . '_single_after_the_content' );

        wp_link_pages( array(
            'before'        => '<div class="page-links">' . esc_html__( 'Pages:', THEME_TEXTDOMAIN ),
            'after'         => '</div>',
            'link_before'   => '<span class="page-links__item">',
            'link_after'    => '</span>',
        ) );
        ?>
    </div><!-- .entry-content -->

</article>

<?php $wptplrb_core->the_option( 'code_after_content' ) ?>

<?php
$source_link = get_post_meta( $post->ID, 'source_link', true );
$source_link = apply_filters( THEME_SLUG . '_post_meta_source_link', $source_link, $post );
$source_hide = get_post_meta( $post->ID, 'source_hide', true );
$source_hide = apply_filters( THEME_SLUG . '_post_meta_source_hide', $source_hide, $post );

if ( ! empty( $source_link ) ) {
    echo '<div class="meta-source">';

    if ( $source_hide == 'checked' ) {
        echo '<span class="ps-link js-link" data-href="' . $source_link .'" data-target="_blank" rel="noopener">' . __( 'Source', THEME_TEXTDOMAIN ) . '</span>';
    } else {
        echo '<a href="'. $source_link .'" target="_blank">' . __( 'Source', THEME_TEXTDOMAIN ) . '</a>';
    }

    echo '</div>';
}
?>


<?php if ( $is_show_tags ) {
	$post_tags = get_the_tags();
	if ( $post_tags ) {
		echo '<div class="entry-tags">';
		foreach( $post_tags as $tag ) {
			echo '<a href="'. get_tag_link( $tag->term_id ) .'" class="entry-tag">'. $tag->name . '</a> ';
		}
		echo '</div>';
	}
} ?>


<?php if ( $is_show_rating && ! $is_show_author_box ) { ?>
    <div class="rating-box">
        <div class="rating-box__header"><?php echo apply_filters( THEME_SLUG . '_single_rating_title', __( 'Rate article', THEME_TEXTDOMAIN ) ) ?></div>
		<?php $post_id = $post ? $post->ID : 0; $wptplrb_rating->the_rating( $post_id, apply_filters( THEME_SLUG . '_rating_text_show', false ) ); ?>
    </div>
<?php } ?>


<?php if ( $is_show_social_bottom ) { ?>
    <div class="entry-social">
		<?php if ( apply_filters( THEME_SLUG . '_single_social_share_title_show', false ) ) : ?>
            <div class="entry-bottom__header"><?php echo apply_filters( THEME_SLUG . '_social_share_title', __( 'Share to friends', THEME_TEXTDOMAIN ) ) ?></div>
		<?php endif; ?>

        <?php do_action( THEME_SLUG . '_single_before_social_bottom' ) ?>
		<?php get_template_part( 'template-parts/blocks/social', 'buttons' ) ?>
        <?php do_action( THEME_SLUG . '_single_after_social_bottom' ) ?>
    </div>
<?php } ?>



<?php if ( $is_show_author_box ) get_template_part( 'template-parts/blocks/author', 'box' ); ?>


<?php if ( ! empty( $thumb ) && ( $thumbnail_type == 'wide' || $thumbnail_type == 'full' || $thumbnail_type == 'fullscreen') ) { ?>
    <meta itemprop="image" content="<?php echo $thumb_url ?>">
    <meta itemprop="headline" content="<?php echo esc_attr( get_the_title() ) ?>">
    <meta itemprop="articleSection" content="<?php get_the_category() ?>">
<?php } ?>
<?php if ( ! $is_show_author ) { ?>
    <meta itemprop="author" content="<?php echo get_the_author() ?>">
<?php } ?>
<meta itemscope itemprop="mainEntityOfPage" itemType="https://schema.org/WebPage" itemid="<?php the_permalink() ?>" content="<?php the_title(); ?>">
<?php if ( ! $is_show_date_modified ) { ?>
    <meta itemprop="dateModified" content="<?php the_modified_time( 'Y-m-d' )?>">
<?php } ?>
<?php if ( ! $is_show_date ) { ?>
    <meta itemprop="datePublished" content="<?php the_time( 'c' ) ?>">
<?php } ?>
<?php echo $wptplrb_template->get_microdata_publisher() ?>
