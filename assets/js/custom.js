(function($) {
    $(document).ready(function() {
		const arr = $("script");
			for (let i=0;i < arr.length;i++) {
				$(arr[i]).attr("defer","defer");
			}
		
        $('.newsfeed__more-button').click(function() {
            let newsfeed = $(this).closest('.newsfeed');
            let category = newsfeed.data('category');
            let page = newsfeed.data('page');
            let pages_count = newsfeed.data('pages-count');
            if (page != pages_count) {

                var data = {
                    page: page+1,
                    category: category
                };

                var api = wpApiSettings.root;

                var request = jQuery.ajax({
                    url: api + 'newsfeed/v1/get_news/',
                    type: "POST",
                    data: data,
                    scriptCharset: "utf-8",
                    success: function (result) {
                        newsfeed.find('.newsfeed__feed').append(result);
                        if (page+1 == pages_count) {
                            newsfeed.find('.newsfeed__more-button-wrapper').remove();
                        }
                    },
                });
            }
        });
		
		 // just for some idea 
		 // 
		
		//функция перевода
		const language ="ru_RU";

		const RULOCALE = {
		  "Moscow": "Москва",
		  "Kazahstan": "Казахстан"
		};

		function translated(language, string){
			if (language.indexOf("ru") > -1) {
				return RULOCALE[string] ? RULOCALE[string] : string;
			} 
			return string; 
		}
		
		//получаем геопозицию
		$.get("https://ipinfo.io", function (response) { 
			const geoWordEl = response.city;
			$('#geoWord').html(translated(language, geoWordEl));
		}, "jsonp");
    });
}(jQuery));


const ActualArticlesSlider = new Swiper('.actual-articles .swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,


    pagination: {
        el: '.actual-articles__pagination .swiper-pagination',
    },


    // Default parameters
    slidesPerView: 3,
    spaceBetween: 15,
    // Responsive breakpoints
    breakpoints: {
        0: {
            slidesPerView: 1,
            spaceBetween: 10
        },
        // when window width is >= 640px
        640: {
            slidesPerView: 2,
            spaceBetween: 15
        },
        // when window width is >= 640px
        968: {
            slidesPerView: 2,
            spaceBetween: 15
        }
    }
});


const CommentsSlider = new Swiper('.last-comments .swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

	navigation: {
		nextEl: '.last-comments .swiper-button-next',
		prevEl: '.last-comments .swiper-button-prev',
	  },

    // Default parameters
    slidesPerView: 1,
    spaceBetween: 15,
});

// Добавление в избранное

(function($) {
    $(document).ready(function() {
        $('.favorite-button').click(function() {
                if ($(this).data('post-id')) {
                    let button = $(this);
                    var api = wpApiSettings.root;

                    var data = {
                        post: $(this).data('post-id')
                    };
                    if (!$(this).hasClass('favorite-button--liked')) {
                        // Добавление в избранное
                        var request = jQuery.ajax({
                            url: api + 'favorites/v1/add_favorite_post/',
                            type: "POST",
                            data: data,
                            scriptCharset: "utf-8",
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
                            },
                            success: function (result) {
                                if (result == 'ok') {
                                    button.addClass('favorite-button--liked');
                                    button.find('.favorite-button__text').html('Удалить из<br><span>избранного</span>');
                                } else if (result == 'user not login') {
                                    // TODO открытые модального окна с авторизацией
                                }
                            },
                        });
                    } else {
                        // Удаление из избранного
                        var request = jQuery.ajax({
                            url: api + 'favorites/v1/remove_favorite_post/',
                            type: "POST",
                            data: data,
                            scriptCharset: "utf-8",
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
                            },
                            success: function (result) {
                                if (result == 'ok') {
                                    button.removeClass('favorite-button--liked');
                                    button.find('.favorite-button__text').html('Добавить в<br><span>избранное</span>');
                                } else if (result == 'user not login') {
                                    // TODO открытые модального окна с авторизацией
                                }
                            },
                        });
                    }
                }
                return false;
        });
    });
}(jQuery));