<?php
/**
 * ****************************************************************************
 *
 *   НЕ РЕДАКТИРУЙТЕ ЭТОТ ФАЙЛ
 *   DON'T EDIT THIS FILE
 *
 *   После обновления Вы потереяете все изменения. Используйте дочернюю тему
 *   After update you will lose all changes. Use child theme
 *
 *   https://support.wptplrb.ru/docs/general/child-themes/
 *
 * *****************************************************************************
 *
 * @package wptplrb
 */

global $wptplrb_core;

get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

            <?php
            /*
            if ( 'top' == $wptplrb_core->get_option( 'structure_home_position' ) ) get_template_part( 'template-parts/content', 'home' );

            $home_constructor = $wptplrb_core->get_option( 'home_constructor' );
            if ( ! empty( $home_constructor ) ) $home_constructor = json_decode( $home_constructor, true );

            if ( ! empty( $home_constructor ) && is_array( $home_constructor ) ) {
	            foreach ( $home_constructor as $section ) {
	                $section_type = ( ! empty( $section['section_type'] ) ) ? $section['section_type'] : 'posts';
		            set_query_var( 'section_options', $section );
		            get_template_part( 'template-parts/sections/' . $section_type );
                }
            } else {

	            if ( have_posts() ) {

                    if ( is_home() && ! is_front_page() ) :
                        echo '<h1 class="page-title screen-reader-text">' . single_post_title( '', false ) . '</h1>';
                    endif;

                    get_template_part( 'template-parts/post-container/' . $wptplrb_core->get_option( 'structure_home_posts' ) );

                    the_posts_pagination();

                } else {

                    get_template_part('template-parts/content', 'none');
                }

            }

            if ( 'bottom' == $wptplrb_core->get_option( 'structure_home_position' ) ) get_template_part( 'template-parts/content', 'home' );
            */

            ?>
            <div class="actual-articles">
                <h2 class="actual-articles__title">Самое читабельное</h2>
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <?php
                        // Блок актуальных новостей
                        // задаем нужные нам критерии выборки данных из БД
                        $args = array(
                            'posts_per_page' => 6,
                            'orderby' => 'meta_value_num',
                            'meta_key' => 'views',
                            'post_type' => 'post',
                        );

                        $query = new WP_Query( $args );

                        // Цикл
                        if ( $query->have_posts() ) {
                            while ( $query->have_posts() ) {
                                $query->the_post();
                                ?>
                                <div class="swiper-slide">
                                <?php
                                    get_template_part( 'template-parts/post-card/slider' );
                                ?>
                                </div>
                                <?php
                            }
                        }
                        // Возвращаем оригинальные данные поста. Сбрасываем $post.
                        wp_reset_postdata();
                        ?>
                    </div>

                    <!-- If we need pagination -->


                </div>
                <div class="actual-articles__pagination">
                    <div class="swiper-pagination"></div>
                </div>
            </div>

            <div class="last-comments">
                <h2 class="last-comments__title">Последние комментарии</h2>
                <div class="last-comments__slider">
                    <div class="last-comments__button-prev swiper-button-prev"></div>
                    <div class="swiper-container">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <?php
                            $args = array(
                                'number'              => '5',
                                'status'              => 'approve',
                            );

                            if( $comments = get_comments( $args ) ){
                                foreach( $comments as $comment ){
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="comment-item">
                                            <a href="<?php echo get_permalink($comment->comment_post_ID); ?>" class="comment-item__avatar">
                                                <img src="<?php echo get_avatar_url($comment->comment_author_email, array(
                                                    'size' => 80,
                                                    'default'=>'wavatar',
                                                )); ?>">
                                            </a>
                                            <div class="comment-item__wrapper">
                                                <div class="comment-item__info">
                                                    <span class="comment-item__author"><?php echo $comment->comment_author; ?></span>
                                                    <span class="comment-item__post-reference">
                                    к посту <a href="<?php echo get_permalink($comment->comment_post_ID); ?>" class="comment-item__post-reference-link"><?php echo get_the_title($comment->comment_post_ID); ?></a>
                                </span>
                                                </div>
                                                <a href="<?php echo get_permalink($comment->comment_post_ID); ?>" class="comment-item__content"><p><?php echo $comment->comment_content; ?></p></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="last-comments__button-next swiper-button-next"></div>
                </div>
            </div>
			
			<h2>Последние новости</h2>
            <?php
                $paged = get_query_var('paged');
                if (!$paged) {
                    $paged = 1;
                }
                $pages_count = Newsfeed::get_pages_count();
            ?>
            <div class="newsfeed" data-category="1" data-page="<?php echo $paged; ?>" data-pages-count="<?php echo $pages_count; ?>">
                <div class="newsfeed__feed">
                    <?php echo Newsfeed::get_page($paged,1); ?>
                </div>
                <nav class="navigation pagination newsfeed__pagination">
                    <div class="screen-reader-text">Навигация по записям</div>
                    <div class="nav-links">
                        <?php
                        if ($paged == $pages_count) {
                        ?>
                            <a class="next page-numbers" href="<?php echo get_site_url();?>/page/<?php echo $pages_count-1; ?>/">Назад</a>
                        <?php
                        }
                        ?>
                        <?php
                         for ($i = 1; $i <= $pages_count; $i++) {
                             if ($i == $paged) {
                                 ?>
                                 <span aria-current="page" class="page-numbers current"><?php echo $i; ?></span>
                                 <?php
                             } else {
                                 ?>
                                 <a class="page-numbers" href="<?php echo get_site_url();?>/page/<?php echo $i; ?>/"><?php echo $i; ?></a>
                                 <?php
                             }
                         }
                         ?>
                        <?php
                        if ($paged != $pages_count) {
                            ?>
                            <a class="next page-numbers" href="<?php echo get_site_url();?>/page/<?php echo $pages_count; ?>/">Далее</a>
                        <?php
                        }
                        ?>
                    </div>
                </nav>
                <?php if ($paged != $pages_count) { ?>
                <div class="newsfeed__more-button-wrapper">
                    <div class="newsfeed__more-button">Ещё...</div>
                </div>
                <?php } ?>

            </div>

		</main><!-- #main -->
	</div><!-- #primary -->

    <?php if ( in_array( $wptplrb_core->get_option( 'structure_home_sidebar' ), [ 'left', 'right' ] ) ) get_sidebar(); ?>

<?php
get_footer();
